import React, { Component } from 'react';
import { QuestionSet } from 'react-quizzical';

class Quiz extends Component {
  render() {
    return (
      <div className="quiz">
        <QuestionSet
          questions={{
            ask: 'Фамилия полицейского надзирателя?',
            choices: [
              {
                text: 'Очумелов',
                then: {
                  ask: 'Из-за какого животного всё случилось?',
                  choices: [
                    {
                      text: 'Лошадь'
                    },
                    {
                      text: 'Хамелеон'
                    },
                    {
                      text: 'Собака',
                      then: {
                        ask: 'Так чья собака?',
                        choices: [
                          {
                            text: 'Генерала'
                          },
                          {
                            text: 'Циган'
                          },
                          {
                            text: 'Брата генерала'
                          },
                          {
                            text: 'Ничья'
                          }
                        ]
                      }
                    },
                    {
                      text: 'Кошка'
                    }
                  ]
                }
              },
              {
                text: 'Ручкин'
              },
              {
                text: 'Бахметьев'
              },
              {
                text: 'Кизяков'
              }
            ]
          }}
          saveButton={() => {
            return <input type="submit" value="ещё раз" />;
          }}
        />
      </div>
    );
  }
}

export default Quiz;
