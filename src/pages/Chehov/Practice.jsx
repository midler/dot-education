import React, { Component } from 'react';
import graph1 from './graph1.png';
import graph2 from './graph2.png';

class Practice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      img: graph1
    };

    this.handlerDown = this.handlerDown.bind(this);
    this.handlerUp = this.handlerUp.bind(this);
  }
  handlerDown() {
    this.setState({ img: graph2 });
    alert('Ниже некуда. Очумелов подстроится под ситуацию');
  }
  handlerUp() {
    this.setState({ img: graph2 });
    alert(
      'Правильно. Он обрадуется что услужил. Хоття дальше видно что собаку ненавидит'
    );
  }
  render() {
    return (
      <div>
        <img src={this.state.img} alt="" style={{ maxWidth: '100%' }} />
        <button onClick={this.handlerUp}>Вверх</button>
        <button onClick={this.handlerUp}>Вниз</button>

        <p>
          Мы узнали что собака принадлежит брату генерала. Как изменится
          отношение персонажа к ней?
        </p>
      </div>
    );
  }
}

export default Practice;
