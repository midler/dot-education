import React, { Component } from 'react';
import { HashRouter as Router, Route, Link } from 'react-router-dom';
import AppBar from 'material-ui/AppBar';

import Home from './Home.jsx';
import Quiz from './Quiz.jsx';
import Practice from './Practice.jsx';

class Index extends Component {
  render() {
    return (
      <Router>
        <div>
          <AppBar position="static">
            <ul component="ul">
              <li style={{ display: 'inline-block' }}>
                <Link
                  to="/chehov/text"
                  style={{
                    color: 'white',
                    textDecoration: 'none',
                    padding: '10px'
                  }}
                >
                  Текст
                </Link>
              </li>
              <li style={{ display: 'inline-block' }}>
                <Link
                  to="/chehov/test"
                  style={{
                    color: 'white',
                    textDecoration: 'none',
                    padding: '10px'
                  }}
                >
                  Тест
                </Link>
              </li>
              <li style={{ display: 'inline-block' }}>
                <Link
                  to="/chehov/practice"
                  style={{
                    color: 'white',
                    textDecoration: 'none',
                    padding: '10px'
                  }}
                >
                  Практика
                </Link>
              </li>
            </ul>
          </AppBar>

          <Route path="/chehov/text" component={Home} />
          <Route path="/chehov/test" component={Quiz} />
          <Route path="/chehov/practice" component={Practice} />
        </div>
      </Router>
    );
  }
}

export default Index;
