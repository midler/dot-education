import React, { Component } from 'react';
import ReactMarkdown from 'react-markdown';
import pageSrc from './text.md';

class Home extends Component {
  render() {
    return <ReactMarkdown source={pageSrc} className="markdown-container" />;
  }
}

export default Home;
